Spyder Frontend

* git clone https://andregce@bitbucket.org/plsos2/spyder-frontend.git
* cd spyder-frontend
* composer install
* cp .env.example .env
* php artisan key:generate
* npm install
* npm run dev
* php artisan migrate:fresh --seed
* php artisan serve